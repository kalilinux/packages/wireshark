Description: idl2deb - create Debian packages from idl2wrs modules
Forwarded: not-needed
Author: W. Borgert <debacle@debian.org>

Index: b/idl2deb
===================================================================
--- /dev/null	1970-01-01 00:00:00.000000000 +0000
+++ b/idl2deb	2010-02-15 17:24:57.000000000 +0100
@@ -0,0 +1,216 @@
+#!/usr/bin/env python
+
+# idl2deb - quick hack by W. Martin Borgert <debacle@debian.org> to create
+# Debian GNU/Linux packages from idl2wrs modules for Wireshark.
+# Copyright 2003, 2008, W. Martin Borgert
+
+# Makefile.am and configure.ac code by:
+# Copyright 2001, Ericsson Inc.
+# Frank Singleton <frank.singleton@ericsson.com>
+#
+# Wireshark - Network traffic analyzer
+# By Gerald Combs <gerald@wireshark.com>
+# Copyright 1998 Gerald Combs
+
+# This program is free software; you can redistribute it and/or
+# modify it under the terms of the GNU General Public License
+# as published by the Free Software Foundation; either version 2
+# of the License, or (at your option) any later version.
+# 
+# This program is distributed in the hope that it will be useful,
+# but WITHOUT ANY WARRANTY; without even the implied warranty of
+# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+# GNU General Public License for more details.
+# 
+# You should have received a copy of the GNU General Public License
+# along with this program; if not, write to the Free Software
+# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
+
+import optparse
+import os
+import string
+import sys
+import time
+
+scriptinfo = """idl2deb version 2008-03-10
+Copyright 2003, 2008, W. Martin Borgert
+Free software, released under the terms of the GPL."""
+
+def bootstrap():
+    """Generate Makefile.in and configure script."""
+    os.system("aclocal")
+    os.system("autoconf")
+    os.system("libtoolize --automake --copy --force")
+    os.system("automake --add-missing --copy --foreign")
+
+def create_file(preserve, filename, content, mode = None):
+    """Create a file with given content."""
+    if preserve and os.path.isfile(filename):
+        return
+    f = open(filename, 'w')
+    f.write(content)
+    f.close()
+    if mode:
+        os.chmod(filename, mode)
+
+def create_files(version, deb, email, idl, name, preserve, iso, rfc):
+    """Create all files for the .deb build process."""
+    base = os.path.basename(idl.lower().split(".idl")[0])
+    create_file(preserve, "Makefile.am", """#
+
+plugindir = @plugindir@
+
+plugin_LTLIBRARIES = %s.la
+%s_la_SOURCES = packet-%s.c
+%s_la_LDFLAGS = -module -avoid-version
+
+GLIB_CFLAGS = `pkg-config --cflags glib-2.0`
+GLIB_LIBS = `pkg-config --libs glib-2.0`
+BUILT_SOURCES = packet-%s.c
+INCLUDES = -DHAVE_CONFIG -DHAVE_SYS_TYPES_H -DHAVE_SYS_TIME_H \\
+    -DHAVE_STDARG_H -D_U_=\"__attribute__((unused))\" \\
+    -I/usr/include/wireshark -DWS_VAR_IMPORT=extern $(GLIB_CFLAGS)
+LDADD = $(GLIB_LIBS)
+
+# Libs must be cleared, or else libtool won't create a shared module.
+# If your module needs to be linked against any particular libraries,
+# add them here.
+LIBS =
+
+%s_la_DEPENDENCIES = packet-%s-static.o
+
+packet-%s-static.o: packet-%s.c
+	$(LTCOMPILE) -c -o packet-%s-static.o \\
+	  -D__WIRESHARK_STATIC__ packet-%s.c
+
+packet-%s.c: """  % ((base,) * 12) + idl + """
+	$(IDL2WRS) -I. $< > $@
+""")
+
+    create_file(preserve, "configure.ac", """AC_INIT(%s, 1.0)
+AC_PROG_LIBTOOL
+AM_INIT_AUTOMAKE
+AM_MAINTAINER_MODE
+AC_PROG_CC
+AC_STDC_HEADERS
+AC_PROG_INSTALL
+AC_SUBST(CFLAGS)
+AC_SUBST(CPPFLAGS)
+AC_SUBST(LDFLAGS)
+PKG_CHECK_MODULES(GLIB, glib-2.0 >= 2.2.2)
+AC_SUBST(GLIB_CFLAGS)
+AC_SUBST(GLIB_LIBS)
+IDL2WRS=\"`type -p idl2wrs`\"
+AC_SUBST(IDL2WRS)
+WIRESHARK_VERSION=\"%s\"
+plugindir=\"$libdir/wireshark/plugins/$WIRESHARK_VERSION\"
+AC_SUBST(plugindir)
+AC_OUTPUT([Makefile])
+""" % (base, version))
+
+    if not os.path.isdir("debian"):
+        os.mkdir("debian")
+
+    create_file(preserve, "debian/rules", """#!/usr/bin/make -f
+
+include /usr/share/cdbs/1/rules/debhelper.mk
+include /usr/share/cdbs/1/class/autotools.mk
+
+PREFIX=`pwd`/debian/wireshark-giop-%s
+
+binary-post-install/wireshark-giop-%s::
+	rm -f $(PREFIX)/usr/lib/wireshark/plugins/%s/*.a
+""" % (base, base, version), 0755)
+
+    create_file(preserve, "debian/control", """Source: wireshark-giop-%s
+Section: net
+Priority: optional
+Maintainer: %s <%s>
+Standards-Version: 3.6.1.0
+Build-Depends: wireshark-dev, autotools-dev, debhelper, cdbs
+
+Package: wireshark-giop-%s
+Architecture: any
+Depends: wireshark (= %s), ${shlibs:Depends}
+Description: GIOP dissector for CORBA interface %s
+ This package provides a dissector for GIOP (General Inter-ORB
+ Protocol) for the Wireshark protocol analyser.  It decodes the CORBA
+ (Common Object Request Broker Architecture) interfaces described
+ in the IDL (Interface Definition Language) file '%s.idl'.
+""" % (base, name, email, base, deb, base, base))
+
+    create_file(preserve, "debian/changelog",
+            """wireshark-giop-%s (0.0.1-1) unstable; urgency=low
+
+  * Automatically created package.
+
+ -- %s <%s>  %s
+""" % (base, name, email, rfc))
+
+    create_file(preserve, "debian/copyright",
+            """This package has been created automatically by idl2deb on
+%s for Debian GNU/Linux.
+
+Wireshark: http://www.wireshark.org/
+
+Copyright:
+
+GPL, as evidenced by existence of GPL license file \"COPYING\".
+(the GNU GPL may be viewed on Debian systems in
+/usr/share/common-licenses/GPL)
+""" % (iso))
+
+def get_wrs_version():
+    """Detect version of wireshark-dev package."""
+    deb = os.popen(
+        "dpkg-query -W --showformat='${Version}' wireshark-dev").read()
+    debv = string.find(deb, "-")
+    if debv == -1: debv = len(deb)
+    version = deb[string.find(deb, ":")+1:debv]
+    return version, deb
+
+def get_time():
+    """Detect current time and return ISO and RFC time string."""
+    currenttime = time.gmtime()
+    return time.strftime("%Y-%m-%d %H:%M:%S +0000", currenttime), \
+           time.strftime("%a, %d %b %Y %H:%M:%S +0000", currenttime)
+
+def main():
+    opts = process_opts(sys.argv)
+    iso, rfc = get_time()
+    version, deb = get_wrs_version()
+    create_files(version, deb,
+                 opts.email, opts.idl, opts.name, opts.preserve,
+                 iso, rfc)
+    bootstrap()
+    os.system("dpkg-buildpackage " + opts.dbopts)
+
+def process_opts(argv):
+    """Process command line options."""
+    parser = optparse.OptionParser(
+        version=scriptinfo,
+        description="""Example:
+%prog -e me@foo.net -i bar.idl -n \"My Name\" -d \"-rfakeroot -uc -us\"""")
+    parser.add_option("-d", "--dbopts",
+                      default="", metavar="opts",
+                      help="options for dpkg-buildpackage")
+    parser.add_option("-e", "--email", metavar="address",
+                      default="invalid@invalid.invalid",
+                      help="use e-mail address")
+    parser.add_option("-i", "--idl", metavar="idlfile",
+                      help="IDL file to use (mandatory)")
+    parser.add_option("-n", "--name", default="No Name",
+                      help="use user name", metavar="name")
+    parser.add_option("-p", "--preserve", action="store_true",
+                      help="do not overwrite files")
+    opts, args = parser.parse_args()
+    if not opts.idl:
+        print "mandatory IDL file parameter missing"
+        sys.exit(1)
+    if not os.access(opts.idl, os.R_OK):
+        print "IDL file not accessible"
+        sys.exit(1)
+    return opts
+
+if __name__ == '__main__':
+    main()
Index: b/idl2deb.dbk
===================================================================
--- /dev/null	1970-01-01 00:00:00.000000000 +0000
+++ b/idl2deb.dbk	2010-02-15 17:24:57.000000000 +0100
@@ -0,0 +1,157 @@
+<?xml version='1.0' encoding='ISO-8859-1'?>
+<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
+"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd" [
+  <!ENTITY command     "<command>idl2deb</command>">
+  <!ENTITY email       "<email>debacle@debian.org</email>">
+  <!ENTITY author      "W. Borgert">
+  <!ENTITY debian      "<productname>Debian GNU/Linux</productname>">
+]>
+
+<refentry>
+  <refentryinfo>
+    <address>
+      &email;
+    </address>
+    <author>
+      <firstname>W.</firstname>
+      <surname>Borgert</surname>
+    </author>
+    <copyright>
+      <year>2003</year>
+      <year>2005</year>
+      <holder>&author;</holder>
+    </copyright>
+    <date>2003-08-17</date>
+    <keywordset>
+      <keyword>CORBA</keyword>
+      <keyword>IDL</keyword>
+      <keyword>Wireshark</keyword>
+    </keywordset>
+    <revhistory>
+      <revision>
+	<revnumber>2003-08-17</revnumber>
+	<date>2003-08-17</date>
+	<authorinitials>debacle</authorinitials>
+	<revremark>First version.</revremark>
+      </revision>
+    </revhistory>
+  </refentryinfo>
+  <refmeta>
+    <refentrytitle>idl2deb</refentrytitle>
+
+    <manvolnum>1</manvolnum>
+  </refmeta>
+  <refnamediv>
+    <refname>idl2deb</refname>
+
+    <refpurpose>create a Debian package for CORBA monitoring from IDL</refpurpose>
+  </refnamediv>
+  <refsynopsisdiv>
+    <cmdsynopsis>
+      &command;
+      <arg><option>-d <replaceable>opts</replaceable></option></arg>
+      <arg><option>--dbopts=<replaceable>opts</replaceable></option></arg>
+      <arg><option>-e <replaceable>address</replaceable></option></arg>
+      <arg><option>--email=<replaceable>address</replaceable></option></arg>
+      <arg>-i <replaceable>idlfile</replaceable></arg>
+      <arg>--idl=<replaceable>idlfile</replaceable></arg>
+      <arg><option>-h</option></arg>
+      <arg><option>--help</option></arg>
+      <arg><option>-n <replaceable>name</replaceable></option></arg>
+      <arg><option>--name=<replaceable>name</replaceable></option></arg>
+      <arg><option>-p</option></arg>
+      <arg><option>--preserve</option></arg>
+      <arg><option>-v</option></arg>
+      <arg><option>--version</option></arg>
+    </cmdsynopsis>
+  </refsynopsisdiv>
+  <refsect1>
+    <title>Description</title>
+
+    <para>This manual page documents briefly the
+      <command>idl2deb</command> command.</para>
+
+    <para>&command; takes an CORBA IDL file as input and creates a
+    Debian package from it.  The package contains a loadable module
+    for the wireshark network analyser.</para>
+  </refsect1>
+  <refsect1>
+    <title>Options</title>
+
+    <variablelist>
+      <varlistentry>
+        <term><option>-d <replaceable>opts</replaceable></option>
+          <option>--dbopts=<replaceable>opts</replaceable></option></term>
+        <listitem>
+          <para>options for dpkg-buildpackage.</para>
+        </listitem>
+      </varlistentry>
+      <varlistentry>
+        <term><option>-e <replaceable>address</replaceable></option>
+          <option>--email=<replaceable>address</replaceable></option></term>
+        <listitem>
+          <para>use e-mail address.</para>
+        </listitem>
+      </varlistentry>
+      <varlistentry>
+        <term><option>-i <replaceable>idlfile</replaceable></option>
+          <option>--idl=<replaceable>idlfile</replaceable></option></term>
+        <listitem>
+          <para>IDL file to use (mandatory).</para>
+        </listitem>
+      </varlistentry>
+      <varlistentry>
+        <term><option>-h</option>
+          <option>--help</option>
+        </term>
+        <listitem>
+          <para>print help and exit.</para>
+        </listitem>
+      </varlistentry>
+      <varlistentry>
+        <term><option>-n <replaceable>name</replaceable></option>
+          <option>--name=<replaceable>name</replaceable></option></term>
+        <listitem>
+          <para>use user name.</para>
+        </listitem>
+      </varlistentry>
+      <varlistentry>
+        <term><option>-p</option>
+          <option>--preserve</option></term>
+        <listitem>
+          <para>do not overwrite files.</para>
+        </listitem>
+      </varlistentry>
+      <varlistentry>
+        <term><option>-v</option>
+          <option>--version</option></term>
+        <listitem>
+          <para>print version and exit.</para>
+        </listitem>
+      </varlistentry>
+    </variablelist>
+
+    <para>Example:</para>
+
+    <programlisting>/usr/bin/idl2deb -e me@foo.net -i bar.idl \
+-n &quot;My Name&quot; -d &quot;-rfakeroot -uc -us&quot;</programlisting>
+
+  </refsect1>
+  <refsect1>
+    <title>See Also</title>
+
+    <para>A lot of tools are used, which you have to
+    <command>apt-get install</command>: wireshark-dev, python,
+    cdbs, autotools-dev, debhelper, dpkg-dev.</para>
+  </refsect1>
+
+  <refsect1>
+    <title>Copying</title>
+
+    <para>This manual page was written by &author; &email; for
+      &debian; (but may be used by others).  Permission is granted to
+      copy, distribute and/or modify this document under the terms of
+      the GNU General Public License, Version 2 or any later
+      version published by the Free Software Foundation.</para>
+  </refsect1>
+</refentry>
