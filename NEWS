Wireshark 1.8.5 Release Notes

   ------------------------------------------------------------------

What is Wireshark?

   Wireshark is the world's most popular network protocol analyzer.
   It is used for troubleshooting, analysis, development and
   education.

What's New

  Bug Fixes

   The following vulnerabilities have been fixed.

     o wnpa-sec-2013-01

       Infinite and large loops in the Bluetooth HCI, CSN.1, DCP-ETSI
       DOCSIS CM-STAUS, IEEE 802.3 Slow Protocols, MPLS, R3, RTPS,
       SDP, and SIP dissectors. Reported by Laurent Butti. (Bugs
       8036, 8037, 8038, 8040, 8041, 8042, 8043, 8198, 8199, 8222)

       Versions affected: 1.8.0 to 1.8.4, 1.6.0 to 1.6.12.

       GENERIC-MAP-NOMATCH

     o wnpa-sec-2013-02

       The CLNP dissector could crash. Discovered independently by
       Laurent Butti and the Wireshark development team. (Bug 7871)

       Versions affected: 1.8.0 to 1.8.4, 1.6.0 to 1.6.12.

       GENERIC-MAP-NOMATCH

     o wnpa-sec-2013-03

       The DTN dissector could crash. (Bug 7945)

       Versions affected: 1.8.0 to 1.8.4, 1.6.0 to 1.6.12.

       GENERIC-MAP-NOMATCH

     o wnpa-sec-2013-04

       The MS-MMC dissector (and possibly others) could crash. (Bug
       8112)

       Versions affected: 1.8.0 to 1.8.4, 1.6.0 to 1.6.12.

       GENERIC-MAP-NOMATCH

     o wnpa-sec-2013-05

       The DTLS dissector could crash. Discovered by Laurent Butti.
       (Bug 8111)

       Versions affected: 1.8.0 to 1.8.4, 1.6.0 to 1.6.12.

       GENERIC-MAP-NOMATCH

     o wnpa-sec-2013-06

       The ROHC dissector could crash. (Bug 7679)

       Versions affected: 1.8.0 to 1.8.4, 1.6.0 to 1.6.12.

       GENERIC-MAP-NOMATCH

     o wnpa-sec-2013-07

       The DCP-ETSI dissector could corrupt memory. Discovered by
       Laurent Butti. (Bug 8213)

       Versions affected: 1.8.0 to 1.8.4, 1.6.0 to 1.6.12.

       GENERIC-MAP-NOMATCH

     o wnpa-sec-2013-08

       The Wireshark dissection engine could crash. Discovered by
       Laurent Butti. (Bug 8197)

       Versions affected: 1.8.0 to 1.8.4, 1.6.0 to 1.6.12.

       GENERIC-MAP-NOMATCH

     o wnpa-sec-2013-09

       The NTLMSSP dissector could overflow a buffer. Discovered by
       Ulf Härnhammar. (Bug X)

       Versions affected: 1.8.0 to 1.8.4, 1.6.0 to 1.6.12.

       GENERIC-MAP-NOMATCH

   The following bugs have been fixed:

     o SNMPv3 Engine ID registration. (Bug 2426)

     o Wrong decoding of gtp.target identification. (Bug 3974)

     o Reassemble.c leaks memory for GLIB > 2.8. (Bug 4141)

     o Wireshark crashes when starting due to out-of-date plugin left
       behind from earlier installation. (Bug 7401)

     o Failed to dissect TLS handshake packets. (Bug 7435)

     o ISUP dissector problem with empty Generic Number. (Bug 7632)

     o Illegal character is used in temporary capture file name. (Bug
       7877)

     o Lua code crashes wireshark after update to 1.8.3. (Bug 7976)

     o Timestamp info is not saved correctly when writing DOS Sniffer
       files. (Bug 7998)

     o 1.8.3 Wireshark User's Guide version is 1.6. (Bug 8009)

     o Core dumped when the file is closed. (Bug 8022)

     o LPP is misspelled in APDU parameter in
       e-CIDMeasurementInitiation request for LPPA message. (Bug
       8023)

     o Wrong packet bytes are selected for ISUP CUG binary code. (Bug
       8035)

     o Decodes FCoE Group Multicast MAC address as Broadcom MAC
       address. (Bug 8046)

     o The SSL dissector stops decrypting the SSL conversation with
       Malformed Packet:SSL error messages. (Bug 8075)

     o Unable to Save/Apply [Unistim Port] in Preferences. (Bug 8078)

     o Some Information Elements in GTPv2 are not dissected
       correctly. (Bug 8079)

     o Wrong bytes highlighted with "Find Packet...". (Bug 8085)

     o 3GPP ULI AVP. SAI is not correctly decoded. (Bug 8098)

     o Wireshark does not show "Start and End Time" information for
       Cisco Netflow/IPFIX with type 154 to 157. (Bug 8105)

     o GPRS Tunnel Protocoll GTP Version 1 does not decode DAF flag
       in Common Flags IE. (Bug 8193)

     o Wrong parcing of ULI of gtpv2 messages - errors in SAC, RAC &
       ECI. (Bug 8208)

     o Version Number in EtherIP dissector. (Bug 8211)

     o Warn Dissector bug, protocol JXTA. (Bug 8212)

     o Electromagnetic Emission Parser parses field Event Id as
       Entity Id. (Bug 8227)

  New and Updated Features

   There are no new features in this release.

  New Protocol Support

   There are no new protocols in this release.

  Updated Protocol Support

   ANSI IS-637-A, ASN.1 PER, AX.25, Bluetooth HCI, CLNP, CSN.1,
   DCP-ETSI, DIAMETER, DIS PDU, DOCSIS CM-STATUS, DTLS, DTN, EtherIP,
   Fibre Channel, GPRS, GTP, GTPv2, HomePlug AV, IEEE 802.3 Slow,
   IEEE 802.15.4, ISUP, JXTA, LAPD, LPPa, MPLS, MS-MMC, NAS-EPS,
   NTLMSSP, ROHC, RSL, RTPS, SDP, SIP, SNMP, SSL

  New and Updated Capture File Support

   DOS Sniffer

Getting Wireshark

   Wireshark source code and installation packages are available from
   http://www.wireshark.org/download.html.

  Vendor-supplied Packages

   Most Linux and Unix vendors supply their own Wireshark packages.
   You can usually install or upgrade Wireshark using the package
   management system specific to that platform. A list of third-party
   packages can be found on the download page on the Wireshark web
   site.

File Locations

   Wireshark and TShark look in several different locations for
   preference files, plugins, SNMP MIBS, and RADIUS dictionaries.
   These locations vary from platform to platform. You can use
   About→Folders to find the default locations on your system.

Known Problems

   Dumpcap might not quit if Wireshark or TShark crashes. (Bug 1419)

   The BER dissector might infinitely loop. (Bug 1516)

   Capture filters aren't applied when capturing from named pipes.
   (Bug 1814)

   Filtering tshark captures with display filters (-R) no longer
   works. (Bug 2234)

   The 64-bit Windows installer does not support Kerberos decryption.
   (Win64 development page)

   Application crash when changing real-time option. (Bug 4035)

   Hex pane display issue after startup. (Bug 4056)

   Packet list rows are oversized. (Bug 4357)

   Summary pane selected frame highlighting not maintained. (Bug
   4445)

   Wireshark and TShark will display incorrect delta times in some
   cases. (Bug 4985)

Getting Help

   Community support is available on Wireshark's Q&A site and on the
   wireshark-users mailing list. Subscription information and
   archives for all of Wireshark's mailing lists can be found on the
   web site.

   Official Wireshark training and certification are available from
   Wireshark University.

Frequently Asked Questions

   A complete FAQ is available on the Wireshark web site.
